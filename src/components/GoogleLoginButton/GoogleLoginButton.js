import React, {Component} from 'react';
import {View, Alert, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';


class GoogleLoginButton extends Component{
    state = {
        isSigninInProgress: false
    }
    
    _signIn = () => {
        GoogleSignin.configure();
        this.signIn();
      }
    
    signIn = async () => {
        try {
          await GoogleSignin.hasPlayServices();
          const userInfo = await GoogleSignin.signIn();
          this.props.saveUser(userInfo);
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
            Alert.alert('SignIn Cancelled');
          } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (f.e. sign in) is in progress already
            Alert.alert('Sign in is already in progress'        );
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
            Alert.alert('Play Services unavailable');
          } else {
            // some other error happened
            Alert.alert('Unknown error');
          }
        }
    };

    signOut = async () => {
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
          this.props.saveUser(null); // Remember to remove the user from your app's state as well
        } catch (error) {
          console.error(error);
        }
    };

    render(){
        let googleButton = <GoogleSigninButton
                            style={styles.googleButton}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                            onPress={this._signIn}
                            disabled={this.state.isSigninInProgress} />

        let userInfo = null;
        if (this.props.userInfo){
            googleButton =  (<TouchableOpacity onPress={this.signOut} style={styles.googleButton}>
                                <Text style={styles.logOutText}>Log Out</Text>
                            </TouchableOpacity>);

            userInfo = (
                <View style={styles.userInfo}>
                <Image
                    style={{width: 100, height: 100}}
                    source={{uri: this.props.userInfo.user.photo}}
                />
                <View style={{paddingLeft: 10}}>
                    <Text>Email: {this.props.userInfo.user.email}</Text>
                    <Text>ID: {this.props.userInfo.user.id}</Text>
                    <Text>Give Name: {this.props.userInfo.user.givenName}</Text>
                    <Text>Family Name: {this.props.userInfo.user.familyName}</Text>
                    <Text>Name: {this.props.userInfo.user.name}</Text>
                </View>
                </View>
          );
        }

        return (
            <View style={styles.container}>
                {googleButton}
                {userInfo}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 10
    },
    userInfo: {
      flexDirection: 'row',
      width: '100%',
      margin: 10,
      padding: 10,
      borderRadius: 5,
      borderStyle: 'solid',
      elevation: 3
    },
    googleButton: {
        width: 198, 
        height: 40,
        elevation: 3,
        borderStyle: 'solid',
        borderRadius: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logOutText:{
        fontWeight: 'bold'
    }
  });

export default GoogleLoginButton;
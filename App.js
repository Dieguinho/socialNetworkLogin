/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, Text, View, Alert, Image} from 'react-native';
import FBLoginButton from './src/components/FBLoginButton/FBLoginButton';
import GoogleLoginButton from './src/components/GoogleLoginButton/GoogleLoginButton';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

type Props = {};
export default class App extends Component<Props> {
  state = {
    isSigninInProgress: false,
    googleUserInfo: null,
    facebookUserInfo: null
  }

  saveFacebookUserInfo = (result) => {
    this.setState({facebookUserInfo: result});
  }

  saveGoogleUserInfo = (result) => {
    this.setState({googleUserInfo: result});
  }

  render() {
    return (
      <View style={styles.container}>
        <FBLoginButton saveUser={this.saveFacebookUserInfo} userInfo={this.state.facebookUserInfo}/>
        <GoogleLoginButton saveUser={this.saveGoogleUserInfo} userInfo={this.state.googleUserInfo}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    padding: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  userInfo: {
    flexDirection: 'row',
    width: '100%',
    margin: 10,
    padding: 10,
    borderRadius: 5,
    borderStyle: 'solid',
    elevation: 3
  }
});

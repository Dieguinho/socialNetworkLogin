import React, { Component } from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { LoginButton, GraphRequest, AccessToken, GraphRequestManager } from 'react-native-fbsdk';

export default class FBLoginButton extends Component {

    async FBGraphRequest(fields, callback) {
        const accessData = await AccessToken.getCurrentAccessToken();
        // Create a graph request asking for user information
        const infoRequest = new GraphRequest('/me', {
            accessToken: accessData.accessToken,
            parameters: {
                fields: {
                    string: fields
                }
            }
        }, callback.bind(this));
        // Execute the graph request created above
        new GraphRequestManager().addRequest(infoRequest).start();
    }

    async FBLoginCallback(error, result) {
        if (error) {
          this.setState({
            showLoadingModal: false,
            notificationMessage: I18n.t('welcome.FACEBOOK_GRAPH_REQUEST_ERROR')
          });
        } else {
          // Retrieve and save user details in state. In our case with 
          // Redux and custom action saveUser
          this.props.saveUser({
            id: result.id,
            email: result.email,
            image: result.picture.data.url,
            lastName: result.last_name,
            firstName: result.first_name,
            name: result.name
          });
        }
      }

    render() {
        let userInfo = null;
        if(this.props.userInfo){
          userInfo = (
            <View style={styles.userInfo}>
              <Image
                style={{width: 100, height: 100}}
                source={{uri: this.props.userInfo.image}}
              />
              <View style={{paddingLeft: 10}}>
                <Text>Email: {this.props.userInfo.email}</Text>
                <Text>ID: {this.props.userInfo.id}</Text>
                <Text>Firstname: {this.props.userInfo.firstName}</Text>
                <Text>Lastname: {this.props.userInfo.lastName}</Text>
                <Text>Name: {this.props.userInfo.name}</Text>
              </View>
            </View>
          );
        }

        return (
            <View style={styles.container}>
                <LoginButton
                    readPermissions={['public_profile']}
                    onLoginFinished={
                        (error, result) => {
                            if (error) {
                                alert("Login failed with error: " + error.message);
                            } else if (result.isCancelled) {
                                alert("Login was cancelled");
                            } else {
                                this.FBGraphRequest('id, email, picture.type(large), first_name, last_name, name', this.FBLoginCallback);
                            }
                        }
                    }
                onLogoutFinished={() => this.props.saveUser(null)}/>
                {userInfo}
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 10
    },
    userInfo: {
      flexDirection: 'row',
      width: '100%',
      margin: 10,
      padding: 10,
      borderRadius: 5,
      borderStyle: 'solid',
      elevation: 3
    }
  });

module.exports = FBLoginButton;